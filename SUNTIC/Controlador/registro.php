<!DOCTYPE html>
<html>
<head>
	
		<link rel="stylesheet" type="text/css" href="..\estilo.css">
	<title></title>
</head>
<body >
<h1><font color="black">REGISTRO</font></h1>

<form method="post" action="" name="signup-form">
<div class="form-element">
<label>Nombres</label>
<input type="text" name="nombres" pattern="[A-Za-z ]*{3,45}" required />
</div>
<div class="form-element">
<label>Apellidos</label>
<input type="text" name="apellidos" pattern="[A-Za-z ]*{3,45}" required />
</div>
<div class="form-element">
<label for="identificacion-select">Tipo De Identificacion</label>
<select name="tipo_id" id="identificacion-select" required>
    <option value="">--Porfavor seleccione una opción--</option>
    <option value="CC">Cedula De Ciudadania</option>
    <option value="CE">Cedula De Extranjeria</option>
    <option value="TI">Tarjeta De Identidad</option>
    <option value="RC">Registro Civil</option>
</select>
</div>
<div class="form-element">
<label>Numero De Identifiacion: </label>
<input type="text" name="numero_id" pattern="[0-9]+" required Title="Solo se aceptan numeros" />
</div>
<div class="form-element">
<label>Numero Fijo o Movil: </label>
<input type="text" name="telefono" pattern="[0-9]+" required Title="Solo se aceptan numeros" />
</div>
<div class="form-element">
<label>Correo electronico: </label>
<input type="email" name="correo" required />
</div>
<div class="form-element">
<label>Confirmar Correo Electronico: </label>
<input type="email" name="conf_correo" required />
</div>
<div class="form-element">
<label>Contraseña: </label>
<input type="password" name="clave" required />
</div>
<div>
<button type="submit" name="registro" value="register">Registrar</button>

<input type="checkbox" data-required="1" name="terminos">
<label style="color:black;"> Aceptar los <a style="color:blue;" href="https://www.google.com/">Términos y Condiciones</a></label>

</div>
<div>
<br>
<a href="..\Controlador\login.php">Iniciar Sesion</a>
</div>
</form>
</body>
</html>



<?php
    session_start();
    include('..\Modelo\conexion.php');
    if (isset($_POST['registro'])) {
		$nombres = $_POST['nombres'];
		$apellidos = $_POST['apellidos'];
		$tipo_id = $_POST['tipo_id'];
        $numero_id = $_POST['numero_id'];
		$telefono = $_POST['telefono'];
        $correo = $_POST['correo'];
		$conf_correo = $_POST['conf_correo'];
        $password = $_POST['clave'];
        $password_hash = md5($password);
		
        $query = $connection->prepare("SELECT * FROM usuarios WHERE numero_id=:numero_id");
        $query->bindParam("numero_id", $numero_id, PDO::PARAM_STR);
        $query->execute();
		if ($query->rowCount() > 0) {
            echo '<script type="text/javascript">
                  window.onload = function () { alert("El usuario ya se encuentra registrado!"); } 
            </script>';
        }
		if ($correo== $conf_correo){
			if(isset($_POST['terminos'])){
			
		if ($query->rowCount() == 0) {
            $query = $connection->prepare("INSERT INTO usuarios(nombres,apellidos,tipo_id,numero_id,telefono,email,clave) VALUES (:nombres,:apellidos,:tipo_id,:numero_id,:telefono,:correo,:clave)");
            $query->bindParam("nombres", $nombres, PDO::PARAM_STR);
			$query->bindParam("apellidos", $apellidos, PDO::PARAM_STR);
			$query->bindParam("tipo_id", $tipo_id, PDO::PARAM_STR);
			$query->bindParam("numero_id", $numero_id, PDO::PARAM_STR);
			$query->bindParam("telefono", $telefono, PDO::PARAM_STR);
			$query->bindParam("correo", $correo, PDO::PARAM_STR);
            $query->bindParam("clave", $password_hash, PDO::PARAM_STR);
            $result = $query->execute();
			
			
			
            if ($result) {
                echo '<script type="text/javascript">
                  window.onload = function () { alert("Registro exitoso!"); } 
            </script>';
            } else {
                echo '<script type="text/javascript">
                  window.onload = function () { alert("Hubo un error,porfavor verifique"); } 
            </script>';
            }
        }
			}else{echo '<script type="text/javascript">
                  window.onload = function () { alert("Alerta!Debe aceptar los terminos y condiciones"); } 
            </script>';}
		}else {
			echo '<script type="text/javascript">
                  window.onload = function () { alert("Los correos no coinciden,porfavor verifique!"); } 
            </script>';
		}
		
		
	}
?>